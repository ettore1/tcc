%Acquiring Body Data using the DepthVideo
%colorVid = videoinput('kinect', 1)
depthVid = videoinput('kinect', 2)
%triggerconfig([colorVid depthVid],'manual');

Source = getselectedsource(depthVid);
Source.EnableBodyTracking = 'on'

preview(depthVid)
    

framesPerTrig = 400;
%colorVid.FramesPerTrigger = 700;
depthVid.FramesPerTrigger = framesPerTrig;

%depthVid.TriggerRepeat = 1;
%colorVid.TriggerRepeat = 1;
flushdata(depthVid, 'triggers')
set(depthVid,'Timeout', 120);

start(depthVid);

%trigger([colorVid depthVid]);

 while get(depthVid,'FramesAvailable')<1  %Wait until at least 1 frame is available
       unavailable=1;
 end

 if get(depthVid,'FramesAvailable')> 0 
    warndlg('Started !')
end
 
[depthImg , time, metadata] = getdata(depthVid);
% [colorFrameData, colorTimeData, colorMetaData] = getdata(colorVid);


% 
% % Create skeleton connection map to link the joints.
SkeletonConnectionMap = [ [4 3];  % Neck
                          [3 21]; % Head
                          [21 2]; % Right Leg
                          [2 1];
                          [21 9];
                          [9 10];  % Hip
                          [10 11];
                          [11 12]; % Left Leg
                          [12 24];
                          [12 25];
                          [21 5];  % Spine
                          [5 6];
                          [6 7];   % Left Hand
                          [7 8];
                          [8 22];
                          [8 23];
                          [1 17];
                          [17 18];
                          [18 19];  % Right Hand
                          [19 20];
                          [1 13];
                          [13 14];
                          [14 15];
                          [15 16];
                        ];

% Extract the 90th frame and tracked body information.
lastFrame = framesPerTrig-10;
lastframeMetadata = metadata(lastFrame);

% Find the indexes of the tracked bodies.
anyBodiesTracked = any(lastframeMetadata.IsBodyTracked ~= 0);
trackedBodies = find(lastframeMetadata.IsBodyTracked);

% Find number of Skeletons tracked.
nBodies = length(trackedBodies);

% Get the joint indices of the tracked bodies with respect to the color
% image.

skeletonJoints = lastframeMetadata.DepthJointIndices(:, :, trackedBodies);
% skeletonJoints = depthMetaData.DepthJointIndices(:, :, trackedBodies);

% Extract the 90th IR frame.
lastColorImage = depthImg(:, :, :, lastFrame);

% Marker colors for up to 6 bodies.
colors = ['r';'g';'b';'c';'y';'m'];

% Display the IR image.
 imshow(lastColorImage);

% Overlay the skeleton on this IR frame.
for i = 1:24
     for body = 1:nBodies
         X1 = [skeletonJoints(SkeletonConnectionMap(i,1),1,body) skeletonJoints(SkeletonConnectionMap(i,2),1,body)];
         Y1 = [skeletonJoints(SkeletonConnectionMap(i,1),2,body) skeletonJoints(SkeletonConnectionMap(i,2),2,body)];
         line(X1,Y1, 'LineWidth', 1.5, 'LineStyle', '-', 'Marker', '+', 'Color', colors(body));
     end

    hold on;
 end
 hold off;
 
 
% stop(depthVid)
% flushdata(depthVid)
% imaqreset
% memory
% clear all