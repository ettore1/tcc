% load C:\Users\jorda\Desktop\Dados_Lab_An_Mov_UFMG\LCA1293_GAIT_02_0005;
% global time LCA1293_GAIT_02_0005
% 
% Frames = (LCA1293_GAIT_02_0005.FrameRate);
% FrameRATE = 1./(LCA1293_GAIT_02_0005.FrameRate);
% FRAME = LCA1293_GAIT_02_0005.Frames;
% LCA1293_GAIT_02_0005.time=(FrameRATE:FRAME)./Frames;
% time= LCA1293_GAIT_02_0005.time;
% A= LCA1293_GAIT_02_0005.Trajectories.Labeled.Data(:,:,:,:);
function [F]=Interpolate_Filter(A)
A=permute(A,[2 3 1]);
% A=permute(A,[1 2 3]);

% Points definition
 P1= A(:,:,1);
 P2= A(:,:,2);
 P3= A(:,:,3);
 P4= A(:,:,4);
 P5= A(:,:,5);
 P6= A(:,:,6);
 P7= A(:,:,7);
 P8= A(:,:,8);
 P9= A(:,:,9);
 P10= A(:,:,10);
 P11= A(:,:,11);
 P12= A(:,:,12);
 P13= A(:,:,13);
 P14= A(:,:,14);
 P15= A(:,:,15);
 P16= A(:,:,16);
 P17= A(:,:,17);
 P18= A(:,:,18);
 P19= A(:,:,19);
 P20= A(:,:,20);
 P21= A(:,:,21);
 P22= A(:,:,22);
 P23= A(:,:,23);
%Find NaN values
[P1,wg1,wge1]= antinan(P1);
[P2,wg2,wge2]= antinan(P2);
[P3,wg3,wge3]= antinan(P3);
[P4,wg4,wge4]= antinan(P4);
[P5,wg5,wge5]= antinan(P5);
[P6,wg6,wge6]= antinan(P6);
[P7,wg7,wge7]= antinan(P7);
[P8,wg8,wge8]= antinan(P8);
[P9,wg9,wge9]= antinan(P9);
[P10,wg10,wge10]= antinan(P10);
[P11,wg11,wge11]= antinan(P11);
[P12,wg12,wge12]= antinan(P12);
[P13,wg13,wge13]= antinan(P13);
[P14,wg14,wge14]= antinan(P14);
[P15,wg15,wge15]= antinan(P15);
[P16,wg16,wge16]= antinan(P16);
[P17,wg17,wge17]= antinan(P17);
[P18,wg18,wge18]= antinan(P18);
[P19,wg19,wge19]= antinan(P19);
[P20,wg20,wge20]= antinan(P20);,
[P21,wg21,wge21]= antinan(P21);
[P22,wg22,wge22]= antinan(P22);
[P23,wg23,wge23]= antinan(P23);



%wg=first frame and wge=last frame
w1=0; w2=0;
for i=1:20;
    w1(1,i)=eval(['wg' num2str(i)]);
end
wg=max(w1);

for i=1:20;
    w2(1,i)=eval(['wge' num2str(i)]);
end
wge=min(w2);
% %Para M4
% wg=4500;
% wge=41500;
% %Para M2
% wg=1900;
% wge=16500;
clear i w1 w2 wg1 wg2 wg3 wg4 wg5 wg5 wg6 wg7 wg8 wg9 wg10 wg11 wg12 wg13 wg14 wg15 wg16 wg17 wg18 wg19 wg20
clear wge1 wge2 wge3 wge4 wge5 wge5 wge6 wge7 wge8 wge9 wge10 wge11 wge12 wge13 wge14 wge15 wge16 wge17 wge18 wge19 wge20

%Interpolation
[a1,b1]= join(P1(:,wg:wge));
[a2,b2]= join(P2(:,wg:wge));
[a3,b3]= join(P3(:,wg:wge));
[a4,b4]= join(P4(:,wg:wge));
[a5,b5]= join(P5(:,wg:wge));
[a6,b6]= join(P6(:,wg:wge));
[a7,b7]= join(P7(:,wg:wge));
[a8,b8]= join(P8(:,wg:wge));
[a9,b9]= join(P9(:,wg:wge));
[a10,b10]= join(P10(:,wg:wge));
[a11,b11]= join(P11(:,wg:wge));
[a12,b12]= join(P12(:,wg:wge));
[a13,b13]= join(P13(:,wg:wge));
[a14,b14]= join(P14(:,wg:wge));
[a15,b15]= join(P15(:,wg:wge));
[a16,b16]= join(P16(:,wg:wge));
[a17,b17]= join(P17(:,wg:wge));
[a18,b18]= join(P18(:,wg:wge));
[a19,b19]= join(P19(:,wg:wge));
[a20,b20]= join(P20(:,wg:wge));
[a21,b21]= join(P21(:,wg:wge));
[a22,b22]= join(P22(:,wg:wge));
[a23,b23]= join(P23(:,wg:wge));

%Joint Interpolated Data
B(1,:,:)=b1(:,:);
B(2,:,:)=b2(:,:);
B(3,:,:)=b3(:,:);
B(4,:,:)=b4(:,:);
B(5,:,:)=b5(:,:);
B(6,:,:)=b6(:,:);
B(7,:,:)=b7(:,:);
B(8,:,:)=b8(:,:);
B(9,:,:)=b9(:,:);
B(10,:,:)=b10(:,:);
B(11,:,:)=b11(:,:);
B(12,:,:)=b12(:,:);
B(13,:,:)=b13(:,:);
B(14,:,:)=b14(:,:);
B(15,:,:)=b15(:,:);
B(16,:,:)=b16(:,:);
B(17,:,:)=b17(:,:);
B(18,:,:)=b18(:,:);
B(19,:,:)=b19(:,:);
B(20,:,:)=b20(:,:);
B(21,:,:)=b21(:,:);
B(22,:,:)=b22(:,:);
B(23,:,:)=b23(:,:);
% clear b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20

%Joint Filtered Data
F(1,:,:)=a1(:,:);
F(2,:,:)=a2(:,:);
F(3,:,:)=a3(:,:);
F(4,:,:)=a4(:,:);
F(5,:,:)=a5(:,:);
F(6,:,:)=a6(:,:);
F(7,:,:)=a7(:,:);
F(8,:,:)=a8(:,:);
F(9,:,:)=a9(:,:);
F(10,:,:)=a10(:,:);
F(11,:,:)=a11(:,:);
F(12,:,:)=a12(:,:);
F(13,:,:)=a13(:,:);
F(14,:,:)=a14(:,:);
F(15,:,:)=a15(:,:);
F(16,:,:)=a16(:,:);
F(17,:,:)=a17(:,:);
F(18,:,:)=a18(:,:);
F(19,:,:)=a19(:,:);
F(20,:,:)=a20(:,:);
F(21,:,:)=a21(:,:);
F(22,:,:)=a22(:,:);
F(23,:,:)=a23(:,:);

F = permute(F,[2 3 1]);
B = permute(B,[2 3 1]);
% clear a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 a11 a12 a13 a14 a15 a16 a17 a18 a19 a20
[Y] = [P1(3,:);P2(3,:);P3(3,:);P4(3,:);P5(3,:);P6(3,:);P7(3,:);P8(3,:);P9(3,:);P10(3,:);P11(3,:);P12(3,:);P13(3,:);P14(3,:);P15(3,:);P16(3,:);P17(3,:);P18(3,:);P19(3,:);P20(3,:);P21(3,:);P22(3,:);P23(3,:)];
[X] = [P1(1,:);P2(1,:);P3(1,:);P4(1,:);P5(1,:);P6(1,:);P7(1,:);P8(1,:);P9(1,:);P10(1,:);P11(1,:);P12(1,:);P13(1,:);P14(1,:);P15(1,:);P16(1,:);P17(1,:);P18(1,:);P19(1,:);P20(1,:);P21(1,:);P22(1,:);P23(1,:)];
[Z] = [P1(2,:);P2(2,:);P3(2,:);P4(2,:);P5(2,:);P6(2,:);P7(2,:);P8(2,:);P9(2,:);P10(2,:);P11(2,:);P12(2,:);P13(2,:);P14(2,:);P15(2,:);P16(2,:);P17(2,:);P18(2,:);P19(2,:);P20(2,:);P21(2,:);P22(2,:);P23(2,:)];



% figure(4)
% scatter((X(1:2,50),Y(1:2,50)),(X(4,50),Y(4,50)));
% title('Joint Mapping')
% xlabel('x(m)')
% ylabel('y(m)') 
% scatter(
% 
% figure(5)
% scatter(X(10,100),Y(10,100));
% title('Joint Mapping')
% xlabel('x(m)')
% ylabel('z(m)') 
% 
% figure(6)
% scatter(y(:,100),z(:,100));
% title('Joint Mapping')
% xlabel('z(m)')
% ylabel('y(m)') 
% 
% % %test data
% % B1=permute(B,[2 3 1]);
% % F1=permute(F,[2 3 1]);
% % % p7=P7(:,wg:wge);
% % X=B1(1,:,7);
% % Y=F1(1,:,7);
% % %Plot all data
% % % figure;plot(P7(1,wg:wge))
% % figure;plot(X(1,:))
% % figure;plot(Y(1,:))
% % %Plot small part
% % % figure;plot(p7(1,2000:5000))
% % figure;plot(X(1,:))
% % figure;plot(Y(1,:))
% % clear B1 F1 p7 X Y 
% 
% %Saving Data
% %QTMmeasurement
% LCA1293_GAIT_02_0005.Frames=LCA1293_GAIT_02_0005.Frames;
% LCA1293_GAIT_02_0005.FrameRate=LCA1293_GAIT_02_0005.FrameRate;
% LCA1293_GAIT_02_0005.wg=wg;
% LCA1293_GAIT_02_0005.wge=wge;
%  LCA1293_GAIT_02_0005.time=(wg:wge)./LCA1293_GAIT_02_0005.FrameRate;
%  Time = LCA1293_GAIT_02_0005.time;
% % %Saving QTM Data 
% %save('C:\JORDANA\Jena\testes\TUIlmenau\FunctMATLAB\Qualisys\FRm2sl', 'QTMmeasurement');
% 
% %Saving interpolated Data 
% %save('C:\JORDANA\Jena\testes\TUIlmenau\FunctMATLAB\Qualisys\Im2sl', 'B', 'QTMmeasurement');
% % save('C:\ANGULOS_ARTICOES\Functional Method\HJC_IF\IF_HJC1R','B');
% 
% %Saving filtered Data 
% %save('C:\JORDANA\Jena\testes\TUIlmenau\FunctMATLAB\Qualisys\Fm2sl', 'F', 'QTMmeasurement');
% 
% Plot data(1-3); interpolation(4-6); Filter(7-9) 
% [graf]=plota(P1,b1,a1);
% plot(Time, P1);
% [graf]=plota(P2,b2,a2);
% % % [graf]=plota(P3,b3,a3);
% % [graf]=plota(P4,b4,a4);
% % [graf]=plota(P5,b5,a5);
% % [graf]=plota(P6,b6,a6);
% % [graf]=plota(P7,b7,a7);
% % [graf]=plota(P8,b8,a8);
% % [graf]=plota(P9,b9,a9);
% % [graf]=plota(P10,b10,a10);
% % [graf]=plota(P11,b11,a11);
% % [graf]=plota(P12,b12,a12);
% % [graf]=plota(P13,b13,a13);
% % [graf]=plota(P14,b14,a14);
% % [graf]=plota(P15,b15,a15);
% % [graf]=plota(P16,b16,a16);
% % [graf]=plota(P17,b17,a17);
% % [graf]=plota(P18,b18,a18);
% % [graf]=plota(P19,b19,a19);
% % [graf]=plota(P20,b20,a20);
% 
