function [NX] = NmedianaFilter(X,nNumber)
    % passar o vetor nDados x quantidade de dados
    % Ex [t, X1, X2, X3, X4];

    for i = 2:size(X,2)
        [nt,nX] = medianaFilter(X(:,1),X(:,i),nNumber);
        if(i==2)
            NX = [nt',nX'];
        else
            NX = [NX,nX'];
        end
    end
    