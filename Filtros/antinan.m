function [y,wg,wge]= antinan (x)
%y: data; wg: first frame used; wge: last frame used; x:data
%Created by Emanuel Andrada
%Last revision 31.5.2005

%Anti NaN Ende

if isnan(x(end)),
    TF=find(isnan (x(1,:)));
    vale_g_e=(TF(2:end)-TF(1:end-1));
    a_g_e=find(vale_g_e~=1);
    

    if a_g_e,
        wge=(TF(a_g_e(end))+vale_g_e(a_g_e(end)))-1;  
        
    
    else 
        wge=TF(1)-1;
 
    end
else wge=length(x); 
    
    end

%x=x(:,1:wge);
%Anti NaN Anfang
p1=x(1,1:end);
TF= find(isnan (p1));
b0= any (TF);
if b0==1,
    vale=(TF(2:end)-TF(1:end-1));
    vale1=find(vale~=1);
    if vale1,
        wg=vale1(1)+1;
    else
        wg=length(TF)+1;
    end
else wg=1;
end
clear TF
y=x;%(:,wg:end);