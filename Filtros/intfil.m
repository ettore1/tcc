function [a,b]=intfil(x)
%a:filtered data for 1 axis; b:interpolated data for 1 axis; x: data for 1 axis
%Created by Emanuel Andrada
%Last revision 14.6.2005
% global QTMmeasurements frequenz

    p1_new=x;
    TF_new= find(isnan (p1_new));
    b1= any (TF_new);
    if b1==0,
        b2 = ones(1,50)/50;             % 10 point averaging filter
        data1f = filtfilt(b2,1,p1_new);           % Noncausal filtering
        at=sgolayfilt(data1f,4,(30-1));
        bt=x;
    end
    if b1==1
        x=1:length(p1_new);
        tt=1:length(p1_new);
        tt(TF_new)=[];
        p1_new(TF_new)=[];
        bt = interp1(tt,p1_new,x,'spline');
        b2 = ones(1,50)/50;             % 10 point averaging filter
        data1f = filtfilt(b2,1,bt);           % Noncausal filtering
        at=sgolayfilt(data1f,4,(30-1));
    end
    %centrado de las seniales
b=bt;%-mean(bt);
a=at;%-mean(at);