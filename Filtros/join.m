function [a1,b1]= join(P1)
%a1:Filtered data for xyz; b1:Interpolated data for xyz
[ax1,bx1]=intfil(P1(1,:));
[ay1,by1]=intfil(P1(2,:));
[az1,bz1]=intfil(P1(3,:));
a1=[ax1;ay1;az1];
b1=[bx1;by1;bz1];
