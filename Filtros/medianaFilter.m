function [nt,nX] = medianaFilter(t,X,nNumber)
    L = length(X);
    
    nt = zeros(1,L-nNumber+1);
    nX = zeros(1,L-nNumber+1);
    
    aux = zeros(1,nNumber);
    n = 1;
    for i = ((nNumber+1)/2):(L-((nNumber-1)/2))
        m = 1;
        for j = (i-((nNumber-1)/2)):(i+((nNumber-1)/2))
            aux(m) = X(j);
            m = m+1;
        end
        aux = sort(aux);
        nt(n) = t(i);
        nX(n) = aux((nNumber+1)/2);
        n = n+1;
    end