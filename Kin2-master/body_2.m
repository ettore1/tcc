
addpath('Mex');
clear all
close all

% Create Kinect 2 object and initialize it
% Available sources: 'color', 'depth', 'infrared', 'body_index', 'body',
% 'face' and 'HDface'
k2 = Kin2('color','depth','body');

% images sizes
%d_width = 512; d_height = 424; outOfRange = 4000;
c_width = 1920; c_height = 1080;

% Color image is to big, let's scale it down
COL_SCALE = 1.0;

% Create matrices for the images
%depth = zeros(d_height,d_width,'uint16');
color = zeros(c_height*COL_SCALE,c_width*COL_SCALE,3,'uint8');

% Create struct array for the body data
p = zeros(3,25); t = [ ]; o = zeros(3,25); trackingst = zeros(1,25); i=1; angl = [ ];
bodydata.Position = p;
bodydata.Orientation = o;
bodydata.Time = t;
bodydata.KneeAngleX = angl;
bodydata.KneeAngleY = angl;
bodydata.KneeAngleZ = angl;
bodydata.HipAngleX = angl;
bodydata.HipAngleY = angl;
bodydata.HipAngleZ = angl;
bodydata.LegLenght = angl;
bodydata.TrackingState = trackingst;


% depth stream figure
%d.h = figure;
%d.ax = axes;
%d.im = imshow(zeros(d_height,d_width,'uint8'));
%hold on;

%title('Depth Source (press q to exit)')
set(gcf,'keypress','k=get(gcf,''currentchar'');'); % listen keypress

% color stream figure
%c.h = figure;
c.ax = axes;
c.im = imshow(color,[]);
title('Color Source (press q to exit)');
set(gcf,'keypress','k=get(gcf,''currentchar'');'); % listen keypress
%hold on

% Loop until pressing 'q' on any figure
k=[];

disp('Press q on any figure to exit')
 %timeframes = [];

while true
    tic
    % Get frames from Kinect and save them on underlying buffer
    validData = k2.updateData;
    
    % Before processing the data, we need to make sure that a valid
    % frame was acquired.
    if validData
        % Copy data to Matlab matrices
        %depth = k2.getDepth;
        color = k2.getColor;

        % update depth figure
        %depth8u = uint8(depth*(255/outOfRange));
        %depth8uc3 = repmat(depth8u,[1 1 3]);
       % d.im = imshow(depth8uc3, 'Parent', d.ax);

        %set(d.im,'CData',depth8uc3); 

        % update color figure
        color = imresize(color,COL_SCALE);
        c.im = imshow(color, 'Parent', c.ax);

        %set(c.im,'CData',color); 

        % Get 3D bodies joints 
        % Input parameter can be 'Quat' or 'Euler' for the joints
        % orientations.
        % getBodies returns a structure array.
        % The structure array (bodies) contains 6 bodies at most
        % Each body has:
        % -Position: 3x25 matrix containing the x,y,z of the 25 joints in
        %   camera space coordinates
        % - Orientation: 
        %   If input parameter is 'Quat': 4x25 matrix containing the 
        %   orientation of each joint in [x; y; z, w]
        %   If input parameter is 'Euler': 3x25 matrix containing the 
        %   orientation of each joint in [Pitch; Yaw; Roll] 
        % -TrackingState: state of each joint. These can be:
        %   NotTracked=0, Inferred=1, or Tracked=2
        % -LeftHandState: state of the left hand
        % -RightHandState: state of the right hand
        [bodies, fcp, timeStamp] = k2.getBodies('Quat');        
        bodydata(i).TrackingState = validData;
        % Number of bodies detected
        numBodies = size(bodies,2);
        %disp(['Bodies Detected: ' num2str(numBodies)])
       
        % Example of how to extract information from getBodies output.
       if numBodies > 0
            % first body info:
            %disp(bodies(1).TrackingState)
            %disp(bodies(1).RightHandState)
            %disp(bodies(1).LeftHandState)
            
            %disp('Right Hand Orientation') % see Kin2.m constants
            %disp(bodies(1).Orientation(:,k2.JointType_HandRight));   
% ------------------------------------  Calculos and displays -----------------            
            
            % Right Lower Limbs
%            disp('Right Knee Position') % see Kin2.m constants
%            disp(bodies(1).Position(:,k2.JointType_KneeRight));
%            disp('Right Ankle Position') % see Kin2.m constants
%            disp(bodies(1).Position(:,k2.JointType_AnkleRight));
%            disp('Right Hip Position') % see Kin2.m constants
%            disp(bodies(1).Position(:,k2.JointType_HipRight));
%             
%             
            % Left lower limbs
            % disp('Left Knee Position') % see Kin2.m constants
             %disp(bodies(1).Position(:,k2.JointType_KneeLeft));
             %disp('Left Ankle Position') % see Kin2.m constants
             %disp(bodies(1).Position(:,k2.JointType_AnkleLeft));
             %disp('Left Hip Position') % see Kin2.m constants
             %disp(bodies(1).Position(:,k2.JointType_HipLeft));
           % disp('Left Leg Lenght')
%            disp(sqrt( (bodies(1).Position(1,k2.JointType_KneeLeft) - bodies(1).Position(1,k2.JointType_AnkleLeft))^2 ...
%    + (bodies(1).Position(2,k2.JointType_KneeLeft) - bodies(1).Position(2,k2.JointType_AnkleLeft))^2 ...
%     + (bodies(1).Position(3,k2.JointType_KneeLeft) - bodies(1).Position(3,k2.JointType_AnkleLeft))^2));
            
         %Euler angles:
%              disp(' Quat') %% knee (y) - Hip (x)
%              disp(bodies(1).Orientation(2, k2.JointType_KneeLeft)-bodies(1).Orientation(1, k2.JointType_HipLeft));
%             
%              disp(bodies(1).Orientation(2, k2.JointType_ElbowLeft)-bodies(1).Orientation(1, k2.JointType_ShoulderLeft));
                
                
            
            % Quaternion Operations ---- Knee
            
            q_K= quaternion(transpose(bodies(1).Orientation(:, k2.JointType_KneeRight)));
            q_A= quaternion(transpose(bodies(1).Orientation(:, k2.JointType_AnkleRight)));
            
            q_Ai = quaternion(quatinv(compact(q_A)));
            
            qK = q_K*q_Ai;
            %qk reprsents the relative quartenion between the knee and the ankle, and 
            %it's where we can extract the angles from the knee
            %disp('Euler Angles Knee')
            eulerAnglesK = eulerd(qK, 'XYZ', 'frame');
            % eulerAngles = quat2eul(q)*(180/pi)
            xrot = eulerAnglesK(1);
            
           % Quaternion Operations ---- Hip
           
           q_H = quaternion(transpose(bodies(1).Orientation(:, k2.JointType_HipRight)));
           %reorienta��o do quaternion do quadril
           [q_HA,q_Hx,q_Hy,q_Hz] = parts(q_H);  % separando as partes do quaternion do quadril
           q_H = quaternion(q_HA, (-1*q_Hz), (-1*q_Hx), q_Hy); % x = y ; y = -x ; z = -z//////////
           q_Ki = quaternion(quatinv(compact(q_K)));
           qH = q_H*q_Ki;
            %qk reprsents the relative quartenion between the knee and the hip, and 
            %it's where we can extract the angles from the hip
            disp('Euler Angles Hip')
           eulerAnglesH = eulerd(qH, 'XYZ', 'frame')
           xrotH=eulerAnglesH(1); 
            %disp('Floor Clip Plane')
            %disp(fcp);
            
            %disp('Body Timestamp')
            %disp(timeStamp);
        
            % To get the joints on depth image space, you can use:
            %pos2D = k2.mapCameraPoints2Depth(bodies(1).Position');
            pos2D = k2.mapCameraPoints2Color(bodies(1).Position'); 
            
            
            % Euler Angle data img
           % dim = [.1 .5 .3 .3];
            tx1 = 'Flex�o extens�o joelho Esquerdo:  '  ;
            tx2 = num2str(xrot);
            tx3 = ' �';
            str = strcat(tx2,tx3);
            %tx4 = 'Flex�o extens�o quadril Esquerdo:  '  
            %tx5 = num2str(xrotH);
            %tx6 = ' �'
            
           % annotation('textbox',dim,'String',str,'FitBoxToText','on');
            text((double(pos2D(14,1))-220),double(pos2D(14,2)),str,'color','yellow','FontSize',20);
            
            bodydata(i).Position = bodies(1).Position(:,:);
            bodydata(i).Orientation = bodies(1).Orientation(:,:);
            bodydata(i).KneeAngleX = eulerAnglesK(1);  % (X Y Z) representation
            bodydata(i).KneeAngleY = eulerAnglesK(2);
            bodydata(i).KneeAngleZ = eulerAnglesK(3);
            bodydata(i).HipAngleX = eulerAnglesH(1);
            bodydata(i).HipAngleY = eulerAnglesH(2);
            bodydata(i).HipAngleZ = eulerAnglesH(3);
            bodydata(i).LegLenght = sqrt( (bodies(1).Position(1,k2.JointType_KneeLeft) - bodies(1).Position(1,k2.JointType_AnkleLeft))^2 ...
   + (bodies(1).Position(2,k2.JointType_KneeLeft) - bodies(1).Position(2,k2.JointType_AnkleLeft))^2 ...
    + (bodies(1).Position(3,k2.JointType_KneeLeft) - bodies(1).Position(3,k2.JointType_AnkleLeft))^2);  % comprimento perna esquerda
            
            % Real time data
%             if isempty(bodydata(i).KneeAngleX)
%                     bodydata(j).KneeAngleX = NaN;
%                     bodydata(j).KneeAngleY = NaN;
%                     bodydata(j).KneeAngleZ = NaN;
%             end
%             
%             
%             figure(2)
%             
%             
%             
       end 
         
        %To get the joints on color image space, you can use:
      %q  pos2D = k2.mapCameraPoints2Color(bodies(1).Position'); 

        % Draw bodies on depth image
        % Parameters: 
        % 1) image axes
        % 2) bodies structure
        % 3) Destination image (depth or color)
        % 4) Joints' size (circle raddii)
        % 5) Bones' Thickness
        % 6) Hands' Size
       % k2.drawBodies(d.ax,bodies,'depth',5,3,15);
        
        % Draw bodies on color image
        k2.drawBodies(c.ax,bodies,'color',10,5,30);
        
    end
    
    timeframes(i) = toc;
    %bodydata(i).Time = timeframes(i) + timeframes(i+1);
    i = i+1;
    % If user presses 'q', exit loop
    if ~isempty(k)
        if strcmp(k,'q'); break; end;
    end
    
    pause(0.02)
end

% building time array in the bodydata stuct
count = 0;
for j=1:length(bodydata)
count = count + timeframes(j);
bodydata(j).Time = count;
end
    
% for j=1:i
%     if isempty(bodydata(j).Orientation) == 1
%         next = isempty(bodydata(j+1).Orientation);
%         next_p1 = isempty(bodydata(j+2).Orientation);
%         if (next & next_p1 == 1)
%             bodydata(j).Orientation = bodydata(j-1).Orientation;
%         end
%         
%         if (next == 0)
%            bodydata(i).Orientation = (bodydata(j-1).Orientation(:,:) + bodydata(j+1).Orientation(:,:))/2;
%         end
%        % bodydata(i).Orientation = (bodydata(j-1).Orientation(:,:) + bodydata(j+1).Orientation(:,:))/2;
%     endq
% end
% Close kinect object
k2.delete;

close all;
