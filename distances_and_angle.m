
% Distances
for i=1:framesPerTrig


%HipLeft to AnkleLeft
H2A(i) = sqrt( (metadata(i).JointPositions(13,1,trackedBodies) - metadata(i).JointPositions(15,1,trackedBodies))^2 ...
    + (metadata(i).JointPositions(13,2,trackedBodies) - metadata(i).JointPositions(15,2,trackedBodies))^2 ...
    + (metadata(i).JointPositions(13,3,trackedBodies) - metadata(i).JointPositions(15,3,trackedBodies))^2);

%HipLeft to KneeLeft
H2K(i) = sqrt( (metadata(i).JointPositions(13,1,trackedBodies) - metadata(i).JointPositions(14,1,trackedBodies))^2 ...
    + (metadata(i).JointPositions(13,2,trackedBodies).^2 - metadata(i).JointPositions(14,2,trackedBodies))^2 ...
    + (metadata(i).JointPositions(13,3,trackedBodies).^2 - metadata(i).JointPositions(14,3,trackedBodies))^2);

%AnkleLeft to KneeLeft
A2K(i) = sqrt( (metadata(i).JointPositions(15,1,trackedBodies) - metadata(i).JointPositions(14,1,trackedBodies))^2 ...
    + (metadata(i).JointPositions(15,2,trackedBodies) - metadata(i).JointPositions(14,2,trackedBodies))^2 ...
    + (metadata(i).JointPositions(15,3,trackedBodies) - metadata(i).JointPositions(14,3,trackedBodies))^2);


% Knee Angle

 %H2A^2 = H2K^2 + A2K^2 - 2*H2K*A2K*cos(x)
 %cos(x) = (H2K^2 + A2K^2 - 2*H2K*A2K)/H2A^2
angle(i) = acosd(cos(((H2K(i).^2 + A2K(i).^2 - 2*H2K(i)*A2K(i))/H2A(i).^2)));

end


%Displacement

for i=1:framesPerTrig
hipR(i) = metadata(i).JointPositions(17,2,trackedBodies);
ankleR(i) = metadata(i).JointPositions(19,2,trackedBodies);
kneeR(i) = metadata(i).JointPositions(18,2,trackedBodies);
end

figure(1)
plot(time, ankleR,'b',time, kneeR,'r', time, hipR, 'g')
figure(2)
plot(time, angle)