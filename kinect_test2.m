% find the kinect 
hwInfo = imaqhwinfo('kinect');
hwInfo.DeviceInfo(1);
hwInfo.DeviceInfo(2);

%set kinect
ColorVid = videoinput('kinect', 1)
DepthVid = videoinput('kinect', 2)

triggerconfig(DepthVid,'manual');
framesPerTrig = 1;
DepthVid.FramesPerTrigger = framesPerTrig;
DepthVid.TriggerRepeat = inf;

DepthSource = getselectedsource(DepthVid)
DepthSource.EnableBodyTracking = 'on'

%Starting video
cam = preview(ColorVid);
start(DepthVid);

% Create skeleton connection map to link the joints.
SkeletonConnectionMap = [ [4 3];  % Neck
                          [3 21]; % Head
                          [21 2]; % Right Leg
                          [2 1];
                          [21 9];
                          [9 10];  % Hip
                          [10 11];
                          [11 12]; % Left Leg
                          [12 24];
                          [12 25];
                          [21 5];  % Spine
                          [5 6];
                          [6 7];   % Left Hand
                          [7 8];
                          [8 22];
                          [8 23];
                          [1 17];
                          [17 18];
                          [18 19];  % Right Hand
                          [19 20];
                          [1 13];
                          [13 14];
                          [14 15];
                          [15 16];
                        ];
                    
while ishandle(cam);

% Get images and metadata from the color and depth device objects.
[colorImg] = getdata(colorVid);
[frame, ts, metaData] = getdata(DepthVid);

% Find the indexes of the tracked bodies.
anyBodiesTracked = any(metaData.IsBodyTracked ~= 0);
trackedBodies = find(metaData.IsBodyTracked);

% Find number of Skeletons tracked.
nBodies = length(trackedBodies);

% Colors for the six bodies 
colors = ['r';'g';'b';'c';'y';'m'];

if  sum(depthMetaData.IsBodyTracked) >0
skeletonJoints = metaData.JointPositions(:,:,metaData.IsBodyTracked)

    for i = 1:24
         for body = 1:nBodies
         X1 = [colorJointIndices(SkeletonConnectionMap(i,1),1,body) colorJointIndices(SkeletonConnectionMap(i,2),1,body)];
         Y1 = [colorJointIndices(SkeletonConnectionMap(i,1),2,body) colorJointIndices(SkeletonConnectionMap(i,2),2,body)];
         line(X1,Y1, 'LineWidth', 1.5, 'LineStyle', '-', 'Marker', '+', 'Color', colors(body));
         end

    
    end
 
end

end

 



