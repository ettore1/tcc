% Antropometria

% Joints
j=1;
% SpineBase = 1;
% SpineMid = 2;
% Neck = 3;
% Head = 4;
% ShoulderLeft = 5;
% ElbowLeft = 6;
% WristLeft = 7;
% HandLeft = 8;
% ShoulderRight = 9;
% ElbowRight = 10;
% WristRight = 11;
% HandRight = 12;
% HipLeft = 13;
% KneeLeft = 14;
% AnkleLeft = 15;
% FootLeft = 16;
% HipRight = 17;
% KneeRight = 18;
% AnkleRight = 19;
% FootRight = 20;
% SpineShoulder = 21;
% HandTipLeft = 22;
% ThumbLeft = 23;
% HandTipRight = 24;
% ThumbRight = 25;

legL(framesPerTrig) = 0;
while find(metadata(j).IsBodyTracked < 1)
    j=j+1;
    
end

trackedBodies = find(metadata(j+1).IsBodyTracked);

%Left Leg

for i=1:framesPerTrig
   legL(i) = sqrt((metadata(i).JointPositions(14, 1, trackedBodies) - metadata(i).JointPositions(15, 1, trackedBodies))^2 ...
       + (metadata(i).JointPositions(14, 2, trackedBodies) - metadata(i).JointPositions(15, 2, trackedBodies))^2....
       + (metadata(i).JointPositions(14, 3, trackedBodies) - metadata(i).JointPositions(15, 3, trackedBodies))^2);
end


% Right Leg

for i=j:framesPerTrig
   legR(i) = sqrt((metadata(i).JointPositions(18, 1, trackedBodies) - metadata(i).JointPositions(19, 1, trackedBodies))^2 ...
       + (metadata(i).JointPositions(18, 2, trackedBodies) - metadata(i).JointPositions(19, 2, trackedBodies))^2....
       + (metadata(i).JointPositions(18, 3, trackedBodies) - metadata(i).JointPositions(19, 3, trackedBodies))^2);
end

% coxa

for i=j:150
   cL(i) = sqrt((metadata(i).JointPositions(13, 1, trackedBodies) - metadata(i).JointPositions(14, 1, trackedBodies))^2 ...
       + (metadata(i).JointPositions(13, 2, trackedBodies) - metadata(i).JointPositions(14, 2, trackedBodies))^2....
       + (metadata(i).JointPositions(13, 3, trackedBodies) - metadata(i).JointPositions(14, 3, trackedBodies))^2);
end

for i=j:150
   legL(i) = sqrt((metadata(i).JointPositions(17, 1, trackedBodies) - metadata(i).JointPositions(18, 1, trackedBodies))^2 ...
       + (metadata(i).JointPositions(17, 2, trackedBodies) - metadata(i).JointPositions(18, 2, trackedBodies))^2....
       + (metadata(i).JointPositions(17, 3, trackedBodies) - metadata(i).JointPositions(18, 3, trackedBodies))^2);
end


    
