%setting up 

hwInfo = imaqhwinfo('kinect');
hwInfo.DeviceInfo(1);
hwInfo.DeviceInfo(2);
    


colorVid = videoinput('kinect',1);
depthVid = videoinput('kinect',2);
triggerconfig ([colorVid depthVid],'manual');
%frames per trigger
colorVid.FramesPerTrigger = 100;
depthVid.FramesPerTrigger = 100;
%trigger Repeat
colorVid.TriggerRepeat = 200;
depthVid.TriggerRepeat = 200;


src = getselectedsource(depthVid);
src.EnableBodyTracking = 'on'

% initialization
start([depthVid colorVid]);
img = figure;
SkeletonConnectionMap = [ [4 3];  % Neck
                          [3 21]; % Head
                          [21 2]; % Right Leg
                          [2 1];
                          [21 9];
                          [9 10];  % Hip
                          [10 11];
                          [11 12]; % Left Leg
                          [12 24];
                          [12 25];
                          [21 5];  % Spine
                          [5 6];
                          [6 7];   % Left Hand
                          [7 8];
                          [8 22];
                          [8 23];
                          [1 17];
                          [17 18];
                          [18 19];  % Right Hand
                          [19 20];
                          [1 13];
                          [13 14];
                          [14 15];
                          [15 16];
                        ];
                    

for i = 1:200
    trigger([colorVid depthVid])
    [imgColor, ts_color, metaData_Color] = getdata(colorVid); 
    [imgDepth, ts_depth, metaData_Depth] = getdata(depthVid);
    %Tracking Bodies
    anyBodiesTracked = any(metaData_Depth.IsBodyTracked ~= 0);
    trackedBodies = find(metaData_Depth.IsBodyTracked);
    nBodies = length(trackedBodies);
    colors = ['r';'g';'b';'c';'y';'m'];
    
    imshow (imgColor, [0 4096]); 
    if sum(metaData_Depth.IsBodyTracked) >0
    skeletonJoints = metaData_Depth.DepthJointIndices (:,:,metaData_Depth.IsBodyTracked);
    hold on;
    for j = 1:24
    for body = 1:nBodies
    X1 = [skeletonJoints(SkeletonConnectionMap(j,1),1,body); skeletonJoints(SkeletonConnectionMap(j,2),1,body)];
    Y1 = [skeletonJoints(SkeletonConnectionMap(j,1),2,body), skeletonJoints(SkeletonConnectionMap(j,2),2,body)];
    line(X1,Y1, 'LineWidth', 2, 'LineStyle', '-' , 'Marker', '+', 'Color', colors(body));
    end
    end
    hold off;
end
    
end
