%Acquiring Body Data using the DepthVideo
%colorVid = videoinput('kinect', 1)
depthVid = videoinput('kinect', 2)
%triggerconfig([colorVid depthVid],'manual');

Source = getselectedsource(depthVid);
Source.EnableBodyTracking = 'on'

%preview(depthVid)
    
%fp=1400;
framesPerTrig = 300;
%colorVid.FramesPerTrigger = 700;
depthVid.FramesPerTrigger = framesPerTrig;

%depthVid.TriggerRepeat = 1;
%colorVid.TriggerRepeat = 1;
flushdata(depthVid, 'triggers')
set(depthVid,'Timeout', 120);

start(depthVid);

%trigger([colorVid depthVid]);

 while get(depthVid,'FramesAvailable')<1  %Wait until at least 1 frame is available
       unavailable=1;
 end

 if get(depthVid,'FramesAvailable')> 0 
    warndlg('Started !')
end
 
[depthImg , time, metadata] = getdata(depthVid);
% [colorFrameData, colorTimeData, colorMetaData] = getdata(colorVid);


% 
% % Create skeleton connection map to link the joints.
SkeletonConnectionMap = [ [4 3];  % Neck
                          [3 21]; % Head
                          [21 2]; % Right Leg
                          [2 1];
                          [21 9];
                          [9 10];  % Hip
                          [10 11];
                          [11 12]; % Left Leg
                          [12 24];
                          [12 25];
                          [21 5];  % Spine
                          [5 6];
                          [6 7];   % Left Hand
                          [7 8];
                          [8 22];
                          [8 23];
                          [1 17];
                          [17 18];
                          [18 19];  % Right Hand
                          [19 20];
                          [1 13];
                          [13 14];
                          [14 15];
                          [15 16];
                        ];
d_width = 512;
d_height = 424;
outRange = 4000;
depth = zeros(d_height,d_width,'uint16');
                    
                    
while depthVid.FramesAcquired > 0 && depthVid.FramesAcquired <= framesPerTrig
    anyBodiesTracked = any(metadata.IsBodyTracked ~= 0);
    trackedBodies = find(metadata.IsBodyTracked);
    nBodies = length(trackedBodies);
    colors = ['r';'g';'b';'c';'y';'m'];
    %imgshw
    imshow(depthImg, [0 4096]);
    if  sum(depthMetaData.IsBodyTracked) > 0
        skeletonJoints = metadata.DepthJointIndices (:,:,metadata.IsBodyTracked);
        hold on
            for i=1:24
                for body = 1:nBodies
                    X1 = [skeletonJoints(SkeletonConnectionMap(i,1),1,body); skeletonJoints(SkeletonConnectionMap(i,2),1,body)];
                    Y1 = [skeletonJoints(SkeletonConnectionMap(i,1),2,body), skeletonJoints(SkeletonConnectionMap(i,2),2,body)];
                    line(X1,Y1, 'LineWidth', 2, 'LineStyle', '-' , 'Marker', '+', 'Color', colors(body));
                end
            end
    end
end
    

% clear all